//versió 0.2


import java.util.Scanner;

/**
 * Crea un nuevo tablero.
 * Mientras haya como mínimo un animal de cada especie, el juego continua.
 * En el momento en que solo haya una especie, el juego acaba y se muestra un mensaje según la especie sobreviviente
 */
public class ConejosVsLobosApp {
    public static void main(String[] args) {
        ConejosVsLobosSim juego = new ConejosVsLobosSim();
        juego.inicializa();
        Scanner scanner = new Scanner(System.in);
        while (juego.hayLobo() && juego.hayConejo()) {
            juego.mostrar();
            for (Animal animal : ConejosVsLobosSim.animales) {
                Animal animalAlrededor = animal.miraAlrededor();
                animal.mover(animalAlrededor);
            }
            ConejosVsLobosSim.addNewAnimals();
            ConejosVsLobosSim.delAnimalInList();
            ConejosVsLobosSim.turnos++;
            scanner.nextLine();
        }
        juego.mostrar();

        if (juego.hayConejo())
            System.out.println("Han sobrevivido los conejos \uD83D\uDC30 , que CRUCKS!");
        else
            System.out.println("Han deborado a los conejos \uD83D\uDC30 , que lobos más rojos \uD83D\uDC3A \n" +
                    "Són rojos porque son carnívoros, es decir, MUCHA sangre, es decir, CuwuNISTES");
    }
}
