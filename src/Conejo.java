//versió 0.2


public class Conejo extends Animal {

    public Conejo(int x, int y) {
        super(x, y);
    }

    @Override
    public String toString() {
        return "\uD83D\uDC30";
    }

    /**
     * Define el movimiento del conejo.
     * Calcula 2 números aleatorios para definir la posiciñon destino.
     * Si animalAlrededor no es nulo, decidirá su movimiento según el tipo de animal que se encuentre (lobo: huir, conejo: reproducirse)
     * Si animalAlrededor es nulo, buscarà cambiar de posición como normalmente lo haría
     *
     * @param animalAlrededor es un animal que se encuentra al rededor del que se va a mover
     */
    @Override
    public void mover(Animal animalAlrededor) {
        Bloque[][] tauler = ConejosVsLobosSim.tablero;
        int i = (int) (Math.random() * 3) - 1 + x;
        int j = (int) (Math.random() * 3) - 1 + y;
        if (animalAlrededor != null && (ConejosVsLobosSim.totalAnimales - ConejosVsLobosSim.conejosIniciales) > ConejosVsLobosSim.conejosTotales()) {
            if (tauler[animalAlrededor.x][animalAlrededor.y].esConejo()) {
                if (ConejosVsLobosSim.dentroTablero(i, j) && !tauler[i][j].esAgua() && !tauler[i][j].esAnimal()) {
                    Animal conillet = new Conejo(i, j);
                    tauler[i][j].setAnimal(conillet);
                    ConejosVsLobosSim.newAnimals.add(conillet);
                    int a = (int) (Math.random() * 3) - 1 + x;
                    int b = (int) (Math.random() * 3) - 1 + y;
                    cambiarPosicion(a, b, tauler);
                }
            } else {
                cambiarPosicion(i, j, tauler);
            }
        } else {
            cambiarPosicion(i, j, tauler);
        }
    }

    /**
     * Cambia la posicion antigua del animal por la nueva.
     *
     * @param i       es la nueva x del animal
     * @param j       es la nueva y del animal
     * @param tablero es la zona de juego donde se encuentran los animales
     */
    public void cambiarPosicion(int i, int j, Bloque[][] tablero) {
        if (ConejosVsLobosSim.dentroTablero(i, j) && !tablero[i][j].esAgua() && !tablero[i][j].esAnimal() && !estaMuerto()) {
            tablero[x][y].delAnimal();
            tablero[i][j].setAnimal(this);
            x = i;
            y = j;
        }
    }
}
