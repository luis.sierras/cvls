//versió 0.2

public abstract class Animal {
    int x, y;
    boolean muerto;


    /**
     * Define la posición donde se encuentra el animal
     *
     * @param x es el eje horizontal en el tablero
     * @param y es el eje vertical en el tablero
     */
    public Animal(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract String toString();

    public abstract void mover(Animal animalAlrededor);

    /**
     * Coloca al animal en la nueva posición introducida, eliminandolo de la posición anterior
     *
     * @param i es la nueva x del animal
     * @param j es la nueva y del animal
     * @param tablero es la zona de juego donde se encuentran los animales
     */
    public abstract void cambiarPosicion(int i, int j, Bloque[][] tablero);

    /**
     * Cambia el estado de muerto a true, por defecto es false
     */
    public void muerto() {
        this.muerto = true;
    }

    /**
     * @return boolean muerto.
     */
    public boolean estaMuerto() {
        return muerto;
    }

    /**
     * Recorre las casillas a su alrededor. Comprueba si se encuentra dentro del tablero y si hay una animal.
     * Cuando pasa por su casilla, automaticamente devuelve al propio animal, por ello se cambia a null.
     *
     * @return animal que se encuentre en una casilla cercana. Si no hay, devuelve null
     */
    public Animal miraAlrededor() {
        Animal animalCerca = null;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (ConejosVsLobosSim.dentroTablero(x + i, y + j)) {
                    if (ConejosVsLobosSim.tablero[x + i][y + j].esAnimal()) {
                        if (ConejosVsLobosSim.tablero[x + i][y + j] == ConejosVsLobosSim.tablero[x][y])
                            animalCerca = null;
                        else {
                            animalCerca = ConejosVsLobosSim.tablero[x + i][y + j].getAnimal();
                            break;
                        }
                    }
                }
            }
        }
        return animalCerca;
    }
}