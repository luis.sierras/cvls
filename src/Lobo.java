//versió 0.2


public class Lobo extends Animal {
    int energia = 15;

    public Lobo(int x, int y) {
        super(x, y);
    }

    @Override
    public String toString() {
        return "\uD83D\uDC3A"; // icono del lobo
    }
    
    /**
     * Define el movimiento del conejo.
     * Calcula 2 números aleatorios para definir la posiciñon destino.
     * Si animalAlrededor no es nulo, decidirá su movimiento según el tipo de animal que encuentre
     * y la cantidad de energia que tenga (conejo: comer, lobo: (poca energia) comer)
     * Si animalAlrededor es nulo, buscarà cambiar de posición como normalmente lo haría
     *
     * @param animalAlrededor es un animal que se encuentra al rededor del que se va a mover
     */
    @Override
    public void mover(Animal animalAlrededor) {
        int i = (int) (Math.random() * 3) - 1 + x;
        int j = (int) (Math.random() * 3) - 1 + y;
        Bloque[][] tauler = ConejosVsLobosSim.tablero;
        if (energia == 0) {
            muerto();
            tauler[x][y].delAnimal();
        } else {
            if (animalAlrededor != null) {
                if (tauler[animalAlrededor.x][animalAlrededor.y].esConejo() && !tauler[animalAlrededor.x][animalAlrededor.y].esRoca()) {
                    comer(tauler, animalAlrededor);
                } else if (tauler[animalAlrededor.x][animalAlrededor.y].esLobo() && energia < 4) {
                    comer(tauler, animalAlrededor);
                } else {
                    cambiarPosicion(i, j, tauler);
                }
            } else {
                cambiarPosicion(i, j, tauler);
            }
        }
    }

    /**
     * Se declara muerto al animal que será comido, se recarga la energia del lobo y se elimina al animal del tablero.
     * El lobo se coloca en la posición en la que se encontraba el animal y se redefinen sus coordenadas.
     *
     * @param tauler es la zona de juego donde se encuentran los animales
     * @param animal es el animal al que se come y el que define la posición final del lobo
     */
    private void comer(Bloque[][] tauler, Animal animal) {
        tauler[animal.x][animal.y].getAnimal().muerto();
        energia = 15;
        tauler[x][y].delAnimal();
        tauler[animal.x][animal.y].setAnimal(this);
        x = animal.x;
        y = animal.y;
    }

    /**
     * Cambiar la posicion antigua del animal por la nueva, en lobo busca una posicion hasta que se pueda mover
     * y no sea la misma que la anterior.
     *
     * @param i       es la nueva x del animal
     * @param j       es la nueva y del animal
     * @param tablero es la zona de juego donde se encuentran los animales
     */
    public void cambiarPosicion(int i, int j, Bloque[][] tablero) {
        if (i == x && j == y) {
            i = (int) (Math.random() * 3) - 1 + x;
            j = (int) (Math.random() * 3) - 1 + y;
            cambiarPosicion(i, j, tablero);
        } else {
            while (i != x || j != y) {
                if (ConejosVsLobosSim.dentroTablero(i, j) && !tablero[i][j].esLobo() && !tablero[i][j].esAgua() && !tablero[i][j].esRoca()) {
                    tablero[x][y].delAnimal();
                    tablero[i][j].setAnimal(this);
                    x = i;
                    y = j;
                    energia -= 1;
                } else {
                    i = (int) (Math.random() * 3) - 1 + x;
                    j = (int) (Math.random() * 3) - 1 + y;
                }
            }
        }
    }
}