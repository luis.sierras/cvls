//versió 0.2


import java.util.ArrayList;

public class ConejosVsLobosSim {
    public static final int CONEJOS = 2, LOBOS = 6, COLUMNAS = 16, FILAS = 16, ROCAS = 10, AGUA = 15;
    public static ArrayList<Animal> animales = new ArrayList<>();
    public static Bloque[][] tablero = new Bloque[COLUMNAS][FILAS];
    public static ArrayList<Animal> newAnimals = new ArrayList<>();
    public static int totalAnimales, turnos = 0;
    public static int conejosIniciales =0;


    /**
     * Crea a partir de números aleatorios la disposición de las casillas y la posición de los animales en el tablero.
     */
    public void inicializa() {
        for (int i = 0; i < COLUMNAS; i++) {
            for (int j = 0; j < FILAS; j++) {
                int terreno = (int) (Math.random() * 100), t = 0;
                int animal = (int) (Math.random() * 100);
                if (terreno < ROCAS) {
                    t = 1;
                } else if (terreno < ROCAS + AGUA) {
                    t = 2;
                }
                Animal a;
                if (t == 2 || animal >= LOBOS + CONEJOS) {
                    tablero[i][j] = new Bloque(t, null);
                } else {
                    if (animal < LOBOS && t != 1) {
                        a = new Lobo(i, j);
                    } else {
                        a = new Conejo(i, j);
                    }
                    animales.add(a);
                    tablero[i][j] = new Bloque(t, a);
                }
            }
        }
        totalAnimales = animales.size();
        conejosIniciales = conejosTotales();

    }

    /**
     * Printa por pantalla el tablero entero, la cantidad de animales y los turnos que se han jugado.
     */
    public void mostrar() {
        for (int i = 0; i < FILAS; i++) {
            for (int j = 0; j < COLUMNAS; j++) {
                System.out.print(tablero[i][j]);
            }
            System.out.println();
        }
        System.out.println("\uD83E\uDDE1" + animales.size() + "/" + totalAnimales + "  \uD83D\uDD50" + turnos);
    }

    /**
     * @param x es la coordenada vertical del tablero
     * @param y es la coordenada horizontal del tablero
     * @return true si las coordenadas se encuentran dentro del tablero
     */
    public static boolean dentroTablero(int x, int y) {
        return x >= 0 && x < tablero.length && y >= 0 && y < tablero[0].length;
    }

    /**
     * Elimina todos los animales muertos de la lista
     */
    public static void delAnimalInList() {
        animales.removeIf(Animal::estaMuerto);
    }

    /**
     * Añade los animales de la lista newAnimals a la lista animales.
     * Finalmente vacia la lista newAnimals
     */
    public static void addNewAnimals() {
        animales.addAll(newAnimals);
        newAnimals.clear();
    }

    /**
     * @return integer con el número total de conejos
     */
    public static int conejosTotales() {
        int total = 0;
        for (Animal animal : animales) {
            if (animal instanceof Conejo) {
                total++;
            }
        }
        return total;
    }

    /**
     * @return true si encuentra un lobo en la lista.
     */
    public boolean hayLobo() {
        for (Animal animal : animales) {
            if (animal instanceof Lobo)
                return true;
        }
        return false;
    }

    /**
     * @return true si encuentra un conejo en la lista.
     */
    public boolean hayConejo() {
        for (Animal animal : animales) {
            if (animal instanceof Conejo)
                return true;
        }
        return false;
    }
}
